<?php

    //Bez tego nic nie pójdzie, tak musi być.
    require_once("config.php");

    try {
        //Próba połączenia się z bazą danych...
        $database = new PDO("mysql:host=".$config["hostname"].";port=".intval($config["port"]).";dbname=".$config["table_name"].";charset=utf8", $config["username"], $config["password"], [
            //A po co to linijki niżej skoro już się połączyliśmy? A po to, aby było bezpiecznie (ochrona przez SQL Injection)
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION

        ]);
    }
    catch (PDOException $error) {
        //Ups, coś nie poszło, muszę dać znać...
        exit("Błąd połączenia z bazą danych: ");
    }
