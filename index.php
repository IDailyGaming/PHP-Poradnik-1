<?php

session_start();

?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Sprawdzanie użytkownika</title>
</head>
<body>
<header>
    <h1>Sprawdzanie użytkownika</h1>
</header>
<main>
    <article>
        <form method="post" action="script.php">
            <label>Nazwa użytkownika <input type="text" name="username"></label>
            <input type="submit" value="Sprawdź!">

            <?php
            if (isset($_SESSION["user_unknown"])) {
                //Ojej, użytkownik nie istnieje, daje znać :(
                echo '<p style="color:red;">Użytkownik nie został odnaleziony!</p>';
                unset($_SESSION["user_unknown"]);
            }
            if (isset($_SESSION["user_known"])) {
                //Yay! Mam użytkownika, daje znać :D
                echo '<p style="color:green;">Użytkownik został odnaleziony!</p>';
                echo '<p>Nazwa użytkownika: <b>'.$_SESSION["username"].'</b><br>';
                echo 'Numer identyfikacyjny: <b>'.$_SESSION["idnum"].'</b><br>';
                echo 'Adres e-mail: <b>'.$_SESSION["email"].'</b><br>';
                echo 'Data rejestracji: <b>'.$_SESSION["regdate"].'</b></p>';
                unset($_SESSION["user_known"]);
                unset($_SESSION["username"]);
                unset($_SESSION["idnum"]);
                unset($_SESSION["email"]);
                unset($_SESSION["regdate"]);
            }
            ?>

        </form>
    </article>
</main>
</body>
</html>