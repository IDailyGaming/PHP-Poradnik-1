<?php

    $config = array(

        //Adres połączenia do bazy danych (np. mysql.mojadomena.pl).
        "hostname" => "",

        //Port połączenia do bazy danych (standardowy to 3306).
        "port" => 3306,

        //Nazwa użytkownika bazy danych (np. banan).
        "username" => "",

        //Hasło użytkownika bazy danych.
        "password" => "",

        //Nazwa tabeli (np. poradnik).
        "table_name" => ""

    );
