<?php

    session_start();

    //Bez tego nic nie pójdzie :P
    require_once("database.php");

    if (isset($_POST["username"])) {
        $username = filter_input(INPUT_POST, "username");
        $query = $database->prepare("SELECT * FROM users WHERE username = :username");
        $query->bindValue(':username', $username, PDO::PARAM_STR);
        $query->execute();
        if (intval($query->rowCount()) == 1) {
            $user = $query->fetch();
            $_SESSION["user_known"] = true;
            $_SESSION["username"] = $user["username"];
            $_SESSION["idnum"] = $user["idnum"];
            $_SESSION["email"] = $user["email"];
            $_SESSION["regdate"] = $user["regdate"];
            header("Location: index.php");
            exit();
        } else {
            $_SESSION["user_unknown"] = true;
            header("Location: index.php");
            exit();
        }
    } else {
        $_SESSION["user_unknown"] = true;
        header("Location: index.php");
        exit();
    }